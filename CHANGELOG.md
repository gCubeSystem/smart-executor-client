This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Smart Executor Client

## [v3.1.0]

- Ported service to authorization-utils [#22871]

## [v3.0.0] 

- Switched smart-executor JSON management to gcube-jackson [#19647]
- Redesigned HTTP APIs to comply with RESTful architectural style [#12997]
- Added API to retrieve scheduled tasks [#10780]


## [v.2.0.0] 

- New version of the client to comply with new REST APIs provided by Smart Executor 2.0.0 [#12997]


## [v1.7.0] [r4.13.1] - 2019-02-26

- Fixed gCoreEndpoint filter using got ServiceEndpoints [#12984]


## [v1.6.0] [r4.10.0] - 2018-02-15

- Added client for REST interface of Smart Executor [#5109]
- Deprecated old SOAP client [#5109]
- Created fallback client presenting as REST client but using SOAP API to support old SmartExecutor instances with new code [#5109]
- Changed pom.xml to use new make-servicearchive directive [#10170]


## [v1.5.0] [r4.6.0] - 2017-07-25

- Exposed unSchedule(executionIdentifier) API


## [v1.4.0] [r4.3.0] - 2017-03-16

- Improved code


## [v1.3.0] [r4.1.0] - 2016-11-07

- SmartExecutor has been migrated to Authorization 2.0 [#4944] [#2112]
- Provided to plugins the possibility to define a custom notifier [#5089]
- Using newly created gcube-bom [#2363]


## [v1.2.0] [r3.10.0] - 2016-02-08

- Added Unscheduling feature for repetitive task [#521]


## [v1.1.0] [r3.9.0] - 2015-12-09

- Added Recurrent and Scheduled Task support [#111]


## [v1.0.0] - 2015-02-05

- First Release

