package org.gcube.vremanagement.executor.client;

import java.util.List;
import java.util.Map;
import java.util.Random;

import org.gcube.vremanagement.executor.client.query.Discover;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SmartExecutorClientFactory {
	
	private static String FORCED_URL = null;
	
	protected static void forceURL(String url) {
		FORCED_URL = url;
	}
	
	public static SmartExecutorClientImpl getClient(String pluginName) {
		return getClient(pluginName, null);
	}
	
	public static SmartExecutorClientImpl getClient(String pluginName, Map<String,String> capabilities) {
		String address;  
		
		if(FORCED_URL != null) {
			address = FORCED_URL;
		}else {
			List<String> addresses = Discover.getInstancesAddress(pluginName, capabilities);
			Random random = new Random();
			int index = random.nextInt(addresses.size());
			address = addresses.get(index);
		}
		
		SmartExecutorClientImpl smartExecutorClient = new SmartExecutorClientImpl();
		smartExecutorClient.setAddress(address);
		smartExecutorClient.setPluginName(pluginName);
		return smartExecutorClient;
	}
	
}
