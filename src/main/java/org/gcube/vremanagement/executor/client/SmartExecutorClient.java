package org.gcube.vremanagement.executor.client;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.plugin.PluginDefinition;
import org.gcube.vremanagement.executor.plugin.PluginStateEvolution;
import org.gcube.vremanagement.executor.plugin.ScheduledTask;

public interface SmartExecutorClient {
	
	public String getHost();
	
	public String getAvailablePlugins();
	
	public List<PluginDefinition> getPlugins() throws IOException;
	
	public String getLaunches();
	
	public List<ScheduledTask> getScheduledLaunches() throws IOException;
	
	public String getOrphanLaunches();
	
	public List<ScheduledTask> getOrphanScheduledLaunches() throws IOException;
	
	public String launch(String launchParameterString);
	
	public UUID launch(LaunchParameter launchParameter);
	
	public String getPluginStateEvolution(String executionIdentifier);
	
	public String getPluginStateEvolution(String executionIdentifier, int iteration);
	
	public PluginStateEvolution getPluginStateEvolution(UUID executionIdentifier);
	
	public PluginStateEvolution getPluginStateEvolution(UUID executionIdentifier, int iteration);
	
	public boolean delete(String executionIdentifier);
	
	public boolean delete(String executionIdentifier, boolean unschedule);
	
	public boolean delete(UUID executionIdentifier);
	
	public boolean delete(UUID executionIdentifier, boolean unschedule);
	
}
