package org.gcube.vremanagement.executor.client.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gcube.common.clients.exceptions.DiscoveryException;
import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.icclient.ICFactory;
import org.gcube.vremanagement.executor.client.Constants;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class Discover {
	
	protected static SimpleQuery getGCoreEndpointQuery() {
		return ICFactory.queryFor(GCoreEndpoint.class)
				.addCondition(String.format("$resource/Profile/ServiceClass/text() eq '%s'", Constants.SERVICE_CLASS))
				.addCondition(String.format("$resource/Profile/ServiceName/text() eq '%s'", Constants.SERVICE_NAME))
				.addCondition("starts-with($resource/Profile/Version/text(),'2')")
				.addCondition(String.format("$resource/Profile/DeploymentData/Status/text() eq 'ready'"))
				.addVariable("$entry", "$resource/Profile/AccessPoint/RunningInstanceInterfaces/Endpoint")
				.addCondition(String.format("$entry/@EntryName eq '%s'", Constants.SERVICE_ENTRY_NAME))
				.setResult("$entry/text()");
	}
	
	protected static SimpleQuery getServiceEndpointQuery() {
		return ICFactory.queryFor(ServiceEndpoint.class)
		.addCondition(String.format("$resource/Profile/Category/text() eq '%s'", Constants.SERVICE_CLASS))
		.addCondition(String.format("$resource/Profile/Name/text() eq '%s'", Constants.SERVICE_NAME))
		.setResult("$resource");
	}
	
	protected static SimpleQuery filterByPluginName(SimpleQuery serviceEndpointQuery, String pluginName) {
		return serviceEndpointQuery.addVariable("$accessPoint", "$resource/Profile/AccessPoint")
				.addCondition(String.format("$accessPoint/Interface/Endpoint/@EntryName eq '%s'", pluginName));
		
	}
	
	protected static SimpleQuery filterByCapabilities(SimpleQuery serviceEndpointQuery, Map<String, String> capabilities) {
		if(capabilities!=null && !capabilities.isEmpty()) {
			int i=0;
			for(String key : capabilities.keySet()) {
				String propertyVariableName = String.format("$property%d", i);
				serviceEndpointQuery.addVariable(propertyVariableName, "$accessPoint/Properties/Property")
						.addCondition(String.format("%s/Name/text() eq '%s'", propertyVariableName, key))
						.addCondition(String.format("%s/Value/text() eq '%s'", propertyVariableName, capabilities.get(key)));
				++i;
			}
		}
		return serviceEndpointQuery;
	}
	
	public static List<String> getInstancesAddress(String pluginName) throws DiscoveryException {
		return getInstancesAddress(pluginName, null);
	}
	
	public static List<String> getInstancesAddress(String pluginName, Map<String, String> capabilities) throws DiscoveryException {
		SimpleQuery serviceEndpointQuery = getServiceEndpointQuery();
		serviceEndpointQuery = filterByPluginName(serviceEndpointQuery, pluginName);
		serviceEndpointQuery = filterByCapabilities(serviceEndpointQuery, capabilities);
		
		List<ServiceEndpoint> serviceEndpoints = ICFactory.clientFor(ServiceEndpoint.class)
				.submit(serviceEndpointQuery);
		
		if(serviceEndpoints.size() == 0) {
			throw new DiscoveryException("No running SmartExecutor wich match the requested conditions");
		}
		
		return getInstancesAddress(serviceEndpoints);
	}
	
	protected static List<String> getInstancesAddress(List<ServiceEndpoint> serviceEndpoints) throws DiscoveryException {
		List<String> addresses = new ArrayList<>();
		for(ServiceEndpoint serviceEndpoint : serviceEndpoints) {
			SimpleQuery gCoreEndpointDiscoveryQuery = getGCoreEndpointQuery();
			String hostname = serviceEndpoint.profile().platform().name();
			gCoreEndpointDiscoveryQuery.addCondition(String.format("contains($entry/string(),'%s')", hostname));
			addresses.addAll(ICFactory.client().submit(gCoreEndpointDiscoveryQuery));
		}
		return addresses;
	}
	
	public static List<String> getInstancesAddress() throws DiscoveryException {	
		SimpleQuery gCoreEndpointDiscoveryQuery = getGCoreEndpointQuery();
		List<String> addresses = ICFactory.client().submit(gCoreEndpointDiscoveryQuery);
		return addresses;
	}
}
