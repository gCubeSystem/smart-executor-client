package org.gcube.vremanagement.executor.client;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Constants {

	/* Used for REST*/
	public static final String SERVICE_CLASS = "org.gcube.vremanagement";
	public static final String SERVICE_NAME = "smart-executor";
	public static final String SERVICE_ENTRY_NAME = "org.gcube.vremanagement.executor.ResourceInitializer";
	
}
