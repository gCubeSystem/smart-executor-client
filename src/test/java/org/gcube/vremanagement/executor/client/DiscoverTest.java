package org.gcube.vremanagement.executor.client;

import java.util.List;

import org.gcube.vremanagement.executor.client.query.Discover;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class DiscoverTest extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(SmartExecutorClientImpl.class);
	
	@Test
	public void testDiscovery() {
		List<String> addresses = Discover.getInstancesAddress();
		logger.debug("Found Smart Executors are available at\n{}", addresses);
	}
	
	@Test
	public void testFilteredDiscovery() {
		String pluginName = "ISExporter";
		List<String> addresses = Discover.getInstancesAddress(pluginName);
		logger.debug("Found Smart Executors with plugin {} are available at\n{}", pluginName, addresses);
	}
}
