package org.gcube.vremanagement.executor.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.client.query.Discover;
import org.gcube.vremanagement.executor.plugin.PluginDefinition;
import org.gcube.vremanagement.executor.plugin.PluginStateEvolution;
import org.gcube.vremanagement.executor.plugin.ScheduledTask;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SmartExecutorClientTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(SmartExecutorClientTest.class);
	
	public static final String HTTP_ADDRESS = "http://smartexecutor.d4science.org:8090/smart-executor/rest";
	public static final String HTTPS_ADDRESS = "https://smartexecutor.d4science.org:8090/smart-executor/rest";
	public static final String HOST = "smartexecutor.d4science.org";
	
	public static final String SLEEP_TIME = "sleepTime";
	public static final String HELLO_WORLD_PLUGIN_NAME = "hello-world-se-plugin";
	public static final String HELLO_WORLD_PLUGIN_VERSION = "2.0.0";
	public static final String HELLO_WORLD_PLUGIN_DESCRIPTION = "Hello World Smart Executor Plugin";
	
	@Test
	public void testSetAddress() {
		SmartExecutorClientImpl smartExecutorClient = new SmartExecutorClientImpl();
		smartExecutorClient.setAddress(HTTP_ADDRESS);
		
		Assert.assertTrue(smartExecutorClient.getAddress().compareTo(HTTP_ADDRESS) == 0);
		Assert.assertTrue(smartExecutorClient.getHost().compareTo(HOST) == 0);
		
		smartExecutorClient.setAddress(HTTPS_ADDRESS);
		Assert.assertTrue(smartExecutorClient.getAddress().compareTo(HTTPS_ADDRESS) == 0);
		Assert.assertTrue(smartExecutorClient.getHost().compareTo(HOST) == 0);
		
	}
	
	private LaunchParameter getLaunchParameter() {
		Scheduling scheduling = new Scheduling(20);
		Map<String,Object> inputs = new HashMap<String,Object>();
		inputs.put(SLEEP_TIME, TimeUnit.SECONDS.toMillis(10));
		inputs.put("TestUUID", UUID.randomUUID());
		
		LaunchParameter launchParameter = new LaunchParameter(HELLO_WORLD_PLUGIN_NAME, inputs);
		launchParameter.setScheduling(scheduling);
		
		return launchParameter;
	}
	
	@Test
	public void testServiceInteraction() throws Exception {
		SmartExecutorClientFactory.forceURL("http://pc-frosini.isti.cnr.it:8080/smart-executor");
		
		String pluginName = HELLO_WORLD_PLUGIN_NAME;
				
		logger.debug("Going to test smart executor using {} plugin", pluginName);
		SmartExecutorClient smartExecutorClient = SmartExecutorClientFactory.getClient(pluginName);
		String host = smartExecutorClient.getHost();
		Assert.assertTrue(host.contains("pc-frosini.isti.cnr.it"));
		
		List<PluginDefinition> plugins = smartExecutorClient.getPlugins();
		Assert.assertTrue(plugins.size() == 1);
		PluginDefinition pluginDefinition = plugins.get(0);
		
		Assert.assertTrue(pluginDefinition.getName().compareTo(pluginName) == 0);
		Assert.assertTrue(pluginDefinition.getDescription().compareTo(HELLO_WORLD_PLUGIN_DESCRIPTION) == 0);
		Assert.assertTrue(pluginDefinition.getVersion().compareTo(HELLO_WORLD_PLUGIN_VERSION) == 0);
		//Assert.assertTrue(pluginDefinition.getSupportedCapabilities().equals(plugin.getSupportedCapabilities()));
		
		List<ScheduledTask> orphans = smartExecutorClient.getOrphanScheduledLaunches();
		for(ScheduledTask orphan : orphans) {
			boolean removed = smartExecutorClient.delete(orphan.getUUID(), true);
			Assert.assertTrue(removed);
		}
		orphans = smartExecutorClient.getOrphanScheduledLaunches();
		Assert.assertTrue(orphans.size() == 0);
		
		LaunchParameter launchParameter = getLaunchParameter();
		
		UUID executionIdentifier = smartExecutorClient.launch(launchParameter);
		
		List<ScheduledTask> launches = smartExecutorClient.getScheduledLaunches();
		Assert.assertTrue(launches.size() == 1);
		ScheduledTask scheduledTask = launches.get(0);
		Assert.assertTrue(scheduledTask.getUUID().compareTo(executionIdentifier) == 0);
		
		PluginStateEvolution pluginStateEvolution = smartExecutorClient.getPluginStateEvolution(executionIdentifier);
		Assert.assertTrue(pluginStateEvolution.getUUID().compareTo(executionIdentifier) == 0);
		
		pluginStateEvolution = smartExecutorClient.getPluginStateEvolution(executionIdentifier, 1);
		Assert.assertTrue(pluginStateEvolution.getUUID().compareTo(executionIdentifier) == 0);
		
		boolean removed = smartExecutorClient.delete(executionIdentifier, true);
		Assert.assertTrue(removed);
		
		orphans = smartExecutorClient.getOrphanScheduledLaunches();
		Assert.assertTrue(orphans.size() == 0);
		
	}
	
	@Test
	public void uiTest() throws IOException {
		List<String> hosts2Contect = Discover.getInstancesAddress();
		boolean first = true;
		for(String host : hosts2Contect) {
			logger.debug("Host {}", SmartExecutorClientImpl.getHostFromCompleteURL(host));
			SmartExecutorClientImpl client = new SmartExecutorClientImpl();
			client.setAddress(host);
			List<PluginDefinition> plugins = client.getPlugins();
			for(PluginDefinition p : plugins) {
				logger.debug("\tPlugin {}", p);
				client.setPluginName(p.getName());
				List<ScheduledTask> tasks = client.getScheduledLaunches();
				for(ScheduledTask t : tasks) {
					logger.debug("\t\tTask {}", t);
				}
				
			}
			if(first) {
				first = false;
				List<ScheduledTask> orphansTasks = client.getOrphanScheduledLaunches();
				for(ScheduledTask t : orphansTasks) {
					logger.debug("Orphan Tasks {}" + t);
				}
			}
		}
	}
	
}
